#!/usr/bin/env bash

ansible-galaxy install -r requirements.yml --force
ansible-playbook erpnext.yml -v --limit="erpnext.webarch.co.uk" "$@"
#ansible-playbook erpnext.yml -v 

