#!/usr/bin/env bash

ansible-galaxy install -r requirements.yml --force
ansible-playbook erpnext.yml -v --limit="dev.erpnext.webarch.co.uk" "$@"

