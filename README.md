## Install ERPNext using Ansible on Debian

This is based on the [manual install](https://github.com/frappe/bench#manual-install) steps.


```bash
ansible-galaxy install -r requirements.yml --force
ansible-playbook erpnext.yml
```
